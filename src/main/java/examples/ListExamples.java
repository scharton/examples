package examples;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ListExamples {
    public List<Long> intersect(List<Long> l1, List<Long> l2) {
        return l1.stream().filter(l2::contains).collect(Collectors.toList());
    }


}
