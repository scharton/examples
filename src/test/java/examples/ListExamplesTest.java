package examples;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListExamplesTest {
    @Test public void testList1() {
        ListExamples listExamples = new ListExamples();
        var ret = listExamples.intersect(Collections.emptyList(), Collections.emptyList());
        Assert.assertTrue(ret.isEmpty());
    }
    @Test public void testList2() {
        ListExamples listExamples = new ListExamples();
        List<Long> l1 = List.of(1L, 2L, 3L);
        List<Long> l2 = List.of(1L, 2L);
        var ret = listExamples.intersect(l1, l2);
        System.out.println(ret);
//        Assert.assertTrue(ret.isEmpty());
    }
    @Test public void testList3() {
        ListExamples listExamples = new ListExamples();
        List<Long> l1 = List.of(1L, 2L);
        List<Long> l2 = List.of(1L, 2L, 3L);
        var ret = listExamples.intersect(l1, l2);
        Assert.assertTrue(ret.size() == 2);
    }
    @Test public void testList4() {
        ListExamples listExamples = new ListExamples();
        List<Long> l1 = List.of(1L, 2L);
        List<Long> l2 = List.of(3L);
        var ret = listExamples.intersect(l1, l2);
        Assert.assertTrue(ret.isEmpty());
    }

    @Test public void testRemoveAll() {
        List<Long> l1 = List.of(1L, 2L, 3L);
        List<Long> l1Mutable = new ArrayList<>(l1);

        List<Long> l2 = List.of(1L, 2L);
        boolean b = l1Mutable.removeAll(l2);
        Assert.assertTrue(b);
        Assert.assertTrue(l1Mutable.size() == 1);
        if (b) {
            System.out.println(l1Mutable);
        }
    }
}
